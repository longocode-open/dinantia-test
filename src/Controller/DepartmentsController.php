<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\ORM\Query;

/**
 * Departments
 */
class DepartmentsController extends AppController
{
    
    /**
     * Base query
     *
     * @return Query
     */
    protected function find() : Query
    {
        return $this->Departments->find();
    }

    /**
     * @return void
     */
    public function index()
    {
        $this->set($this->find()->toArray());
    }

}
