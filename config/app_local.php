<?php

return [
    'debug' => true,

    'Security' => [
        'salt' => env('SECURITY_SALT', '586d4463546ccdaa481ca6858472f156e413b30bdeb063e3b2516d9c4d97ef2d'),
    ],

    'Datasources' => [
        'default' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Sqlite',
            'persistent' => false,
            'host' => 'localhost',
            'username' => null,
            'password' => null,
            'database' => 'dinantia.db',
            'encoding' => 'utf8',
            'timezone' => 'UTC',
        ],
    ],
];
