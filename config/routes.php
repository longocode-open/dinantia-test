<?php
use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;

$routes->setRouteClass(DashedRoute::class);

$routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);
$routes->connect('/pages/*', 'Pages::display');

$routes->scope('/', function (RouteBuilder $builder) {
    $builder->setExtensions(['json']);
    $builder->fallbacks();
});
